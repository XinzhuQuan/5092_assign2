﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4
{
    public class Simulator
    {
        public double S;
        public double K;
        public double R;
        public double Sigma;
        public double T;
        public double trail;
        public static double[] Mon()
        {
            //double[,] arr = new double[CommonValue.trail, Convert.ToInt32(CommonValue.Time) * 252];
            double sum = 0;
            double sum_delta = 0;
            double sum_deltain = 0;
            double sum_deltade = 0;
            double sum_gamma = 0;
            double sum_vega = 0;
            double sum_rho = 0; 
            double sum_theta = 0;
            double sum_ave = 0;
            double sum_a = 0;
           
            

            for (int i = 0; i < CommonValue.trail*(1+CommonValue.indicator); i++)
            {

                European Eu = new European(i);//sum every trails' price
                Eu.GetPrice();
                CommonValue.Price[i] = Eu.price;//store the option price 
                sum = sum + Eu.price;// use eu.price
                sum_deltain = sum_deltain + Eu.price_in;//same
                sum_deltade = sum_deltade + Eu.price_de;
                sum_theta = sum_theta + Eu.price_theta;
                sum_vega = sum_vega + Eu.price_vega;
                sum_rho = sum_rho + Eu.price_rho;

            }
            sum_ave = sum / (CommonValue.trail * (1 + CommonValue.indicator)); //calculate the average
            sum = sum * Math.Exp(-CommonValue.Time * CommonValue.Rate) / (CommonValue.trail * (1 + CommonValue.indicator)); //discount the option price

            if (CommonValue.indicator ==1)//if it is artithetic
            {

               for (int i = 0; i < CommonValue.trail; i++)
                {
                CommonValue.Price[i] = 0.5*(CommonValue.Price[i] + CommonValue.Price[i + CommonValue.trail]);  //change the option price
                 }
            }


            for (int i = 0; i < CommonValue.trail; i++)
            {

                sum_a = sum_a + (CommonValue.Price[i] - sum_ave) * (CommonValue.Price[i] - sum_ave); // in order to calculate the sdv

            }

           
            sum_a = Math.Sqrt(sum_a / (CommonValue.trail * (CommonValue.trail - 1))) * Math.Exp(-CommonValue.Time * CommonValue.Rate);// calculate the sdv 

            if (CommonValue.indicator == 1)
            {
                CommonValue.trail = (CommonValue.trail * (1 + CommonValue.indicator));
            }
            sum_deltain = sum_deltain * Math.Exp(-CommonValue.Time * CommonValue.Rate) / CommonValue.trail;//discount the increase price
            sum_deltade = sum_deltade * Math.Exp(-CommonValue.Time * CommonValue.Rate) / CommonValue.trail;//discount the decrease price
            sum_delta = (sum_deltain - sum_deltade) / (2 * CommonValue.increR);//calclulate the delta
            sum_gamma = (sum_deltain + sum_deltade - 2 * sum) / (CommonValue.increR * CommonValue.increR);//calculate the gamma
            sum_vega = sum_vega * Math.Exp(-CommonValue.Time * CommonValue.Rate) / CommonValue.trail;//discount
            sum_vega = (sum_vega - sum) / CommonValue.increS; //calculate
            sum_rho = sum_rho * Math.Exp(-CommonValue.Time * (CommonValue.increS + CommonValue.Rate)) / CommonValue.trail;//discount
            sum_rho = (sum_rho - sum) / CommonValue.increS;//calculate
            sum_theta = sum_theta * Math.Exp(-(CommonValue.Time +1.0/CommonValue.steps) * CommonValue.Rate) / CommonValue.trail;//discount
            sum_theta = -(sum_theta - sum) / (1.0/CommonValue.steps);//calculate



            double[] result=new double[7] { sum,sum_delta,sum_gamma,sum_theta,sum_vega,sum_rho,sum_a };//output 
            return result;
        }

    }
}

