﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp4;
namespace WindowsFormsApp4
{
    //create a class to put some universal variables
    static class CommonValue
    { //take the number from the users
        static public double Spotprice;
        static public double Strike;
        static public int Time;
        static public double Sigma;
        static public double Rate;
        static public int trail;
        static public int steps;
   
        static public double[,] rand;
        //random matrix
        static public double[,] St;
        //stock matrix
        static public double[] Price;
        static public int indicator;
        static public int stt;
        static public double increS = 0.01;
        static public double increR = 0.1;
       


        static public double type;
        static public void gene_path()//create value in the matrix
        {
            stt = Convert.ToInt32(Math.Round(1.0 * (CommonValue.Time * CommonValue.steps)));
            St = new double[CommonValue.trail * (1 + CommonValue.indicator), stt + 2];
            rand = new Fancyrandom().randlist;
            Price = new double[CommonValue.trail*(1+CommonValue.indicator)];
        }
    }
}
